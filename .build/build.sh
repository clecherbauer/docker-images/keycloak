#!/bin/bash
# shellcheck disable=SC1091
# shellcheck disable=SC1090
# shellcheck disable=SC2001
set -e

microdnf update
microdnf install curl ca-certificates

# CREATE FAKE YUM TO SATISFY mariadb_repo_setup which does not longer support microdnf (SCRIPTCEPTION)
cat > /usr/local/bin/yum <<EOF
#!/bin/bash
echo curl
echo ca-certificates
EOF
chmod +x /usr/local/bin/yum

curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | bash -s -- --skip-maxscale
# REMOVE FAKE YUM
rm /usr/local/bin/yum

#curl -sS https://yum.mariadb.org/RPM-GPG-KEY-MariaDB > /etc/pki/rpm-gpg/MariaDB-Server-GPG-KEY
##curl -sS https://yum.mariadb.org/RPM-GPG-KEY-MariaDB > /etc/pki/rpm-gpg/MariaDB-MaxScale-GPG-KEY
#curl -sS https://yum.mariadb.org/RPM-GPG-KEY-MariaDB > /etc/pki/rpm-gpg/MariaDB-Enterprise-GPG-KEY

rpm --import https://yum.mariadb.org/RPM-GPG-KEY-MariaDB

# install libpmem
curl http://mirror.centos.org/centos/8-stream/AppStream/x86_64/os/Packages/libpmem-1.6.1-1.el8.x86_64.rpm --output libpmem.rpm
rpm -i libpmem.rpm
rm -f libpmem.rpm

microdnf update
microdnf install findutils MariaDB-client
